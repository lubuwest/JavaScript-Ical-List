﻿JavaScript ICAL List Viewer
---------------------
A script to show events from .ics (Icalendar) files using JavaScript.

**Usage Example:**

		show_event_list('sample.ics',5,'SUMMARY','DESCRIPTION','start_date','start_time');

The first argument must be the location of the ICal File. The second argument must be the maximum number of events to be shown.
The arguments after that specify the elements of the event in the order of their appearance. Every element appears in a \<span\> Tag with a class name corresponding to the element name.
In CSS they can be configured similar to the example index.html. The ::before and ::after selector can be used to include commas and dashes.
The following elements are possible: 'day' (day of the week), 'start_time', 'end_time', 'start_date', 'end_date', 'start_date_long', 'SUMMARY', 'DESCRIPTION'.
The site must contain an element \<ul\> with id="calendar".
The extra file de.js is needed for German translation. If it is not included, Months and Weekdays are in English. If you have other language files, please make pull request!

ICal files can be produced by Thunderbird with the [Lightning](https://addons.mozilla.org/de/thunderbird/addon/lightning/) Addon, Owncloud with the [Calendar+](https://github.com/libasys/calendarplus) Addon or Friendica with the [Cal](https://github.com/friendica/friendica-addons/tree/master/cal) Addon.

		
**License:**

	MIT (No attribution required):
	
	Copyright (C) 2012 Carl Saggs
	
	Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.